# MVT - Generátor terénu pomocí Perlin noise v Unity
![image.png](./image.png) 
<p>Generuje Noise mapu pomocí perlin noise + lacunarity + persistance. Jestli je zapnutý "Island mode" generuje také cirkulární gradient, který odečítá od noise mapy. Následně vytvoří texturu v závislosti na zadaném gradientu (v inspectoru) s 2 módy (fixní a prolínání barev). Vytvoří triangle mesh a nastaví pozici jednotlivých vertexů v závislosti na hodnotě v noise mapě a height scale. </p>
