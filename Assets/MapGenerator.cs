using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEditor.AssetImporters;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class MapGenerator : MonoBehaviour
{
    [Range(1, 255)]
    public int mapWidth = 255;
    public int seed { get; set; }
    public float noiseScale { get; set; } = 100f;
    public int octaves { get; set; } = 5;
    public float persistance { get; set; } = 0.5f;
    public float lacunarity { get; set; } = 2.5f;

    public enum DrawMode { NoiseMap, ColorMap, Mesh, FallOff };
    public DrawMode drawMode = DrawMode.Mesh;
    public bool useFallOff { get; set; } = false;
    public float fallOffScale { get; set; } = 1.5f;

    public float heightMultiplier { get; set; } = 250;
    public AnimationCurve meshHeightCurve;

    public bool autoUpdate = true;

    public Vector2 offset = Vector2.zero;


    public Gradient colorGradient;
    public bool Blend { get; set; } = false;
    public float groundLevel { get; set; } = 0f;


    void Start()
    {
        Debug.Log("StartMapgen");
    }

    void Update()
    {
        GenerateMap();
    }

    public void GenerateMap()
    {
        //Generate noise map with given parameters
        float[,] noiseMap = Noise.GenerateNoiseMap(mapWidth, mapWidth, seed, noiseScale, octaves, persistance, lacunarity, offset);
        float[,] fallOffMap = new float[,];

        if(useFallOff){
            fallOffMap = FalloffGen.GenerateFalloffMap(mapWidth, fallOffScale);
        }

        MapDisplay display = FindObjectOfType<MapDisplay>();

        Color[] colorMap = new Color[mapWidth * mapWidth];

        if (Blend)
        {
            colorGradient.mode = GradientMode.Blend;
        }
        else
        {
            colorGradient.mode = GradientMode.Fixed;
        }

        for (int y = 0; y < mapWidth; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {   
                // check if using falloff for island generation
                if (useFallOff)
                {
                    noiseMap[x, y] = Mathf.Clamp(noiseMap[x, y] - fallOffMap[x, y], 0.0f, 1.0f);
                }
                // clamps values depending on groundlevel
                if (noiseMap[x, y] < groundLevel)
                {
                    noiseMap[x, y] = groundLevel;
                }
                // eveluate color depending on gradient
                colorMap[y * mapWidth + x] = colorGradient.Evaluate(noiseMap[x, y]);
            }
        }

        // drawMode switch for debugging
        switch (drawMode)
        {
            case DrawMode.NoiseMap:
                display.DrawTexture(TextureGenerator.TextureFromHeightMap(noiseMap));
                break;
            case DrawMode.ColorMap:
                display.DrawTexture(TextureGenerator.TextureFromColormap(colorMap, mapWidth, mapWidth));
                break;
            case DrawMode.Mesh:
                display.DrawMesh(MeshGenerator.GenerateMesh(noiseMap, heightMultiplier, meshHeightCurve), TextureGenerator.TextureFromColormap(colorMap, mapWidth, mapWidth));
                break;
            case DrawMode.FallOff:
                display.DrawTexture(TextureGenerator.TextureFromHeightMap(fallOffMap));
                break;
        }


    }
}