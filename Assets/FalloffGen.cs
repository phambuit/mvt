using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public static class FalloffGen
{
    
    public static float[,] GenerateFalloffMap(int size, float islandSize)
    {
        float[,] map = new float[size, size];
        float midPoint = size / 2f;
        float maxDistanceToCentre = Mathf.Sqrt(midPoint * midPoint + midPoint * midPoint);
        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                float a = x - midPoint;
                float b = y - midPoint;

                float distanceToCentre = Mathf.Sqrt(a * a + b * b);

                map[x, y] = Mathf.Exp(distanceToCentre * 12 * islandSize / maxDistanceToCentre - 6);
            }
        }
        return map;
    }
}
