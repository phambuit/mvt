using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Noise
{
    // generate noise map from given parameters using perlin noise
    public static float[,] GenerateNoiseMap(int width, int height, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset)
    {
        if (width == 0) { width = 1; }
        if (height == 0) { height = 1; }
        float[,] noiseMap = new float[width, height];
        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        System.Random rng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];
        for (int i = 0; i < octaves; i++)
        {
            float randOffsetX = rng.Next(-10000, 100000) + offset.x;
            float randOffsetY = rng.Next(-10000, 100000) + offset.y;
            octaveOffsets[i] = new Vector2(randOffsetX, randOffsetY);
        }

        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                float amplitude = 1;
                float frequency = 1;
                float noiseHeight = 0;
                for (int i = 0; i < octaves; i++)
                {
                    float tmpX = (x - (width / 2f)) / scale * frequency + octaveOffsets[i].x;
                    float tmpY = (y - (height / 2f)) / scale * frequency + octaveOffsets[i].y;
                    float perlinValue = Mathf.PerlinNoise(tmpX, tmpY) * 2 - 1;
                    noiseHeight += perlinValue * amplitude;

                    amplitude *= persistance;
                    frequency *= lacunarity;
                }

                if (noiseHeight > maxNoiseHeight) { maxNoiseHeight = noiseHeight; }
                if (noiseHeight < minNoiseHeight) { minNoiseHeight = noiseHeight; }
                noiseMap[x, y] = noiseHeight;
            }
        }

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                noiseMap[i, j] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, noiseMap[i, j]);
            }
        }
        return noiseMap;
    }
}
