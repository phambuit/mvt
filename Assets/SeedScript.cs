using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeedScript : MonoBehaviour
{
    [SerializeField] MapGenerator mapGenerator;
    [SerializeField] Slider slider_;
    // Start is called before the first frame update
    void Start()
    {
        slider_.onValueChanged.AddListener((v) =>
        {
            mapGenerator.seed = ((int)v);
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
